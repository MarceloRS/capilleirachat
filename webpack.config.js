'use strict';

var path         = require('path')
  , webpack      = require('webpack')
  , node_modules = path.resolve(__dirname, 'node_modules')
  , config;

config = {
  entry     : {
    app : [ 'webpack-dev-server/client?http://0.0.0.0:8080', 'webpack/hot/only-dev-server', './app/app.js' ]
  },
  output    : {
    path     : path.resolve(__dirname, 'build'),
    filename : 'bundle.js'
  },
  module    : {
    loaders : [
      { test : /\.js$/, loader : 'react-hot!babel?optional[]=es7.decorators', exclude : /node_modules/ },
      { test : /\.scss$/, loader : 'style!css!sass' }
    ]
  }
};

module.exports = config;
