import alt from '../alt';
import io from 'socket.io-client';

let socket;

@alt.createActions
class ChatActions {

  constructor () {
    this.generateActions('newMessage', 'newInformationMessage');
  }

  connect (config) {
    const userInfo = { name : config.user, room : config.room };
    socket = io.connect(config.socketUrl);

    // Connect to the server in dealer or player mode.
    if (config.mode === 'dealer') {
      socket.emit('add dealer', userInfo);
    }
    else if (config.mode === 'player') {
      socket.emit('add player', userInfo);
    }

    // Bind socket listeners.
    socket.on('new dealer', (name) => {
      ChatActions.newInformationMessage(`Dealer has changed to ${name}.`)
    });

    socket.on('new player', (name) => {
      ChatActions.newInformationMessage(`+ ${name} has joined the game.`)
    });

    socket.on('new message', (message) => {
      ChatActions.newMessage(message);
    });

    socket.on('player leaved', (name) => {
      ChatActions.newInformationMessage(`- ${name} has leaved the game.`)
    });
  }

  addMessage (message) {
    socket.emit('add message', message);
    return message;
  }

}

export default ChatActions;
