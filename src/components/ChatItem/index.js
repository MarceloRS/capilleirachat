import React from 'react';
import EasyStyle from 'react-easy-style';

// Styles
import style from './style.scss';

@EasyStyle( style )
class ChatItem extends React.Component {

  constructor (props) {
    super(props);
  }

  render () {
    let item;
    const { message, player, me, info } = this.props;

    if (info) {
      item = <li>
              <span is="message">{message}</span>
            </li>;
    }
    else if (me) {
      item = <li>
              <span is="player-name">Me:</span>
              <span is="message">{message}</span>
            </li>;
    }
    else {
      item = <li>
              <span is="player-name">{player}</span>
              <span is="message">{message}</span>
            </li>;
    }

    return item;
  }
}

export default ChatItem;
