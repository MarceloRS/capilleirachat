// Third parties.
import React from 'react';
import connectToStores from 'alt/utils/connectToStores';
import EasyStyle from 'react-easy-style';

// Actions & Stores.
import ChatActions from '../../actions/ChatActions';
import ChatStore from '../../stores/ChatStore';

// UI Components.
import ChatItem from '../ChatItem';
import ChatForm from '../ChatForm';
import MinusPlusButtons from '../DealerButtons';
import VerticalButtons from '../PlayerButtons';

// Styles
import style from './style.scss';

@connectToStores
@EasyStyle( style )
class ChatView extends React.Component {

  static getStores () {
    return [ ChatStore ];
  }

  static getPropsFromStores () {
    return ChatStore.getState();
  }

  constructor (props) {
    super(props);
    this.addMessage = this.addMessage.bind(this);
    ChatActions.connect({ socketUrl : this.props.socket, mode : this.props.mode, room : this.props.room, user : this.props.user });
  }

  addMessage (text) {
    if (text.length) {
      let lastMessage = React.findDOMNode(this.refs.messages);
      ChatActions.addMessage(text);
      lastMessage.scrollTop = lastMessage.scrollHeight;
    }
  }

  render () {
    /*
    * Dummy Data when chat is down
    * TODO: Remove this when completely unnecessary
    *
    this.props.chatMessages = [
      {
        info: true,
        me: false,
        player: 'Alberto',
        message: 'info: true me: false, este es un mensaje de info'
      },
      {
        info: false,
        me: true,
        player: 'Alberto',
        message: 'info: false me: true, este es un mensaje que yo veo como mio'
      },
      {
        info: false,
        me: false,
        player: 'Someone Else',
        message: 'info: false me: false este es un mensaje que no es mio'
      },
      {
        info: true,
        me: false,
        player: 'Alberto',
        message: 'info: true me: false, este es un mensaje de info'
      },
      {
        info: false,
        me: true,
        player: 'Alberto',
        message: 'info: false me: true, este es un mensaje que yo veo como mio'
      },
      {
        info: false,
        me: false,
        player: 'Someone Else',
        message: 'info: false me: false este es un mensaje que no es mio'
      },
      {
        info: true,
        me: false,
        player: 'Alberto',
        message: 'info: true me: false, este es un mensaje de info'
      },
      {
        info: false,
        me: true,
        player: 'Alberto',
        message: 'info: false me: true, este es un mensaje que yo veo como mio'
      },
      {
        info: false,
        me: false,
        player: 'Someone Else',
        message: 'info: false me: false este es un mensaje que no es mio'
      }
    ];*/

    let messages = this.props.chatMessages.map((message) => {
      return <ChatItem info={message.info} me={message.me} player={message.player} message={message.message} />;
    }), chatForm, hr, dealerPlayerMessages, dealerPlayerBox, minusPlusButtons, verticalButtons;

    if (this.props.mode === 'player') {
      verticalButtons = <VerticalButtons />
      dealerPlayerMessages = <ul is="player-messages" ref="messages">{messages}</ul>;
      hr = <hr />;
      chatForm = <ChatForm onAddMessage={this.addMessage} />;
      dealerPlayerBox = <div is="chat-player">{verticalButtons}{dealerPlayerMessages}{hr}{chatForm}</div>
    }

    if (this.props.mode === 'dealer') {
      minusPlusButtons = <MinusPlusButtons />
      dealerPlayerMessages = <ul is="dealer-messages" ref="messages">{messages}</ul>;
      dealerPlayerBox =  <div is="chat-dealer"> {minusPlusButtons} {dealerPlayerMessages}</div>
    }

    return <div>
      {dealerPlayerBox}
    </div>;
  }
}

export default ChatView;
