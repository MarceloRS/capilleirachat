// Third parties.
import React from 'react';
import EasyStyle from 'react-easy-style';

// Styles
import style from './style.scss';

@EasyStyle( style )
class MinusPlusButtons extends React.Component {
  render() {
    return <div>
        <button is="btn"> + </button>
        <button is="btn"> &minus; </button>
      </div>
  }
}

export default MinusPlusButtons;
