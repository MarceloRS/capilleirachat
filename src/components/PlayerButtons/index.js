// Third parties.
import React from 'react';
import EasyStyle from 'react-easy-style';

// Styles
import style from './style.scss';

@EasyStyle( style )
class VerticalButtons extends React.Component {
  render() {
    return <div>
        <button is="btn-gold">Chat</button>
        <button is="btn-gold">Live Dealer Lobby</button>
      </div>
  }
}

export default VerticalButtons;
